const passport = require("passport");
const passportJWT = require("passport-jwt");

const ExtractJWT = passportJWT.ExtractJwt;

let JWTStrategy = null;
JWTStrategy = passportJWT.Strategy;

const opts = {
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET_KEY,
};

const strategy = new JWTStrategy(opts, (payload, next) => {
  if (payload && payload.currentUser) {
    return next(null, payload.currentUser);
  } else {
    return next(null, false);
  }
});

passport.use(strategy);

module.exports = passport;
