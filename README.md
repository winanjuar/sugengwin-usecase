## How to run (using docker)

Unduh repository ke dalam perangkat local menggunakan perintah `git clone`.
Url repository dapat dilihat di dalam repository yang diinginkan.

```
git clone https://gitlab.com/winanjuar/sugengwin-usecase.git
```

Masuk ke folder project dengan menggunakan perintah `cd`

```
cd sugengwin-usecase
```

Build image dengan menggunakan perintah `docker build -t`

```
docker build -t sugengwin/api-usecase .
```

Running container/service api dan database dengan `docker compose`

```
docker compose up -d
```

Masuk ke container database, karena belum ada DBnya, kita akan membuat database "usecase" disana.

```
docker exec -it db-usecase /bin/sh
```

Jalankan perintah berikut

```
mysql -uroot -ppassword
create database usecase;
exit;
```

Jangan lupa untuk exit sekali lagi, untuk keluar dari shell container database

```
exit
```

Load sampel data yang sudah disiapkan pada file usecasedb_dump.sql

```
docker exec -i db-usecase mysql -uroot -ppassword usecase < usecasedb_dump.sql
```

Akses menggunakan browser atau postman

```
http://localhost:3000
```

## How to run (using npm start)

Unduh repository ke dalam perangkat local menggunakan perintah `git clone`.
Url repository dapat dilihat di dalam repository yang diinginkan.

```
git clone https://gitlab.com/winanjuar/sugengwin-usecase.git
```

Masuk ke folder project dengan menggunakan perintah `cd`

```
cd sugengwin-usecase
```

Buatlah file .env (bisa diambil dari .env.example yang sudah disiapkan). Sesuaikan dengan configurasi akses service database yang ada di local. Buatlah database usecase di service mariadb yang sedang running.

Agar dapat dijalankan, pertama kali `npm install` untuk menginstall dependency, kemudian jalankan `sequelize` untuk migrasi dan seeder. Jika `sequelize` belum diinstall secara global, dapat menggunakan `npx`

```
npm install
sequelize db:migrate
sequelize db:seed:all

```

Untuk merunning service gunakan perintah `npm start`

```
npm start
```

Akses menggunakan browser atau postman, sesuai port yang disetting pada file .env. Jika tidak, secara default menggunakan port 3000.

```
http://localhost:3000
```

Untuk menjalankan unit test, gunakan perintah `npm run test`

```
npm run test
```

Untuk melihat coverage, gunakan perintah `npm run coverage`

```
npm run coverage
```

## Asumsi

Stock product selalu tersedia (unlimited)
User dapat melakukan checkout sebagian
User dapat melakukan checkout multi merchant
Merchant, User, Product dan Promo tanpa proses DML, data hanya berasal dari seeder/initial data

## List end point

| Method | End Point          | Auth  | Deskripsi                                                                                                                                       |
| ------ | ------------------ | ----- | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| GET    | /product/v1?\*     | Tidak | untuk melakukan pencarian product. Query yang dapat dikirim page, sortBy, sortDir, limit, merchantId, search, priceFrom, priceTo, dan hasPromo. |
| POST   | /user/v1/login     | Tidak | untuk melakukan login. Body yang dapat dikirim email dan password.                                                                              |
| POST   | /cart/v1           | Ya    | untuk memilih product dan memasukkan ke keranjang. Body yang dapat dikirimkan productId dan quantity.                                           |
| GET    | /cart/v1           | Ya    | untuk melihat list product di keranjang belanja.                                                                                                |
| POST   | /order/v1/checkout | Ya    | untuk melakukan checkout. Body yang dapat dikirimkan cartIds (array)                                                                            |
| GET    | /order/v1          | Ya    | untuk melihat daftar transaksi yang sudah dilakukan                                                                                             |
| GET    | /order/v1/:id      | Ya    | untuk melihat info detail transaksi yang sudah dilakukan. Param adalah id transaksi.                                                            |

## Selamat Mencoba
