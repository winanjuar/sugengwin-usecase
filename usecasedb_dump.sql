-- MariaDB dump 10.19  Distrib 10.7.3-MariaDB, for debian-linux-gnu (aarch64)
--
-- Host: localhost    Database: usecase
-- ------------------------------------------------------
-- Server version	10.7.3-MariaDB-1:10.7.3+maria~focal

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+07:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Carts`
--

DROP TABLE IF EXISTS `Carts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `merchantId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `promoId` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `fixBuy` int(11) DEFAULT NULL,
  `free` int(11) DEFAULT NULL,
  `fixFree` int(11) DEFAULT NULL,
  `fixQuantity` int(11) DEFAULT NULL,
  `bruto` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Carts`
--

LOCK TABLES `Carts` WRITE;
/*!40000 ALTER TABLE `Carts` DISABLE KEYS */;
INSERT INTO `Carts` VALUES
(1,1,1,1,1,6000,8,6,2,2,6,48000,12000,36000,'2022-04-03 07:57:04','2022-04-03 07:57:04');
/*!40000 ALTER TABLE `Carts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Merchants`
--

DROP TABLE IF EXISTS `Merchants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Merchants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Merchants`
--

LOCK TABLES `Merchants` WRITE;
/*!40000 ALTER TABLE `Merchants` DISABLE KEYS */;
INSERT INTO `Merchants` VALUES
(1,'monserrat_beahan_official','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(2,'onie_haag17_official','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(3,'gertrude8_official','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(4,'angelita16_official','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(5,'lavon.dickinson1_official','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL);
/*!40000 ALTER TABLE `Merchants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OrderItems`
--

DROP TABLE IF EXISTS `OrderItems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OrderItems` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `orderId` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `infoCartId` int(11) DEFAULT NULL,
  `merchantId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `promoId` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `fixBuy` int(11) DEFAULT NULL,
  `free` int(11) DEFAULT NULL,
  `fixFree` int(11) DEFAULT NULL,
  `fixQuantity` int(11) DEFAULT NULL,
  `bruto` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `netto` double DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `orderId` (`orderId`),
  KEY `merchantId` (`merchantId`),
  KEY `productId` (`productId`),
  KEY `promoId` (`promoId`),
  CONSTRAINT `OrderItems_ibfk_1` FOREIGN KEY (`orderId`) REFERENCES `Orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrderItems_ibfk_2` FOREIGN KEY (`merchantId`) REFERENCES `Merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrderItems_ibfk_3` FOREIGN KEY (`productId`) REFERENCES `Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `OrderItems_ibfk_4` FOREIGN KEY (`promoId`) REFERENCES `Promos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OrderItems`
--

LOCK TABLES `OrderItems` WRITE;
/*!40000 ALTER TABLE `OrderItems` DISABLE KEYS */;
INSERT INTO `OrderItems` VALUES
('77d1fc0a-b229-45fa-a6e4-c47069575b74','12026caa-f92f-4680-a24a-f57fd2fa31b5',2,1,2,NULL,6000,2,2,0,0,2,12000,0,12000,'2022-04-03 07:57:04','2022-04-03 07:57:04'),
('84661bc0-9eb8-4af1-8637-efeaa81eda64','12026caa-f92f-4680-a24a-f57fd2fa31b5',1,1,1,1,6000,4,3,1,1,4,24000,6000,18000,'2022-04-03 07:57:04','2022-04-03 07:57:04');
/*!40000 ALTER TABLE `OrderItems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Orders`
--

DROP TABLE IF EXISTS `Orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Orders` (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `orderNumber` varchar(100) DEFAULT NULL,
  `orderTime` datetime DEFAULT NULL,
  `grandBruto` double DEFAULT NULL,
  `grandDiscount` double DEFAULT NULL,
  `grandNetto` double DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`),
  CONSTRAINT `Orders_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Orders`
--

LOCK TABLES `Orders` WRITE;
/*!40000 ALTER TABLE `Orders` DISABLE KEYS */;
INSERT INTO `Orders` VALUES
('12026caa-f92f-4680-a24a-f57fd2fa31b5',1,'ORD220403145721','2021-12-21 22:45:33',36000,6000,30000,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL);
/*!40000 ALTER TABLE `Orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Products`
--

DROP TABLE IF EXISTS `Products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `merchantId` int(11) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merchantId` (`merchantId`),
  CONSTRAINT `Products_ibfk_1` FOREIGN KEY (`merchantId`) REFERENCES `Merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Products`
--

LOCK TABLES `Products` WRITE;
/*!40000 ALTER TABLE `Products` DISABLE KEYS */;
INSERT INTO `Products` VALUES
(1,'Intelligent Frozen Computer','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',6000,'http://loremflickr.com/600/600/cats?89663',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(2,'Ergonomic Rubber Cheese','The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',6000,'http://loremflickr.com/600/600/cats?8006',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(3,'Practical Soft Computer','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',19500,'http://loremflickr.com/600/600/cats?419',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(4,'Tasty Plastic Shirt','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',8000,'http://loremflickr.com/600/600/cats?15739',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(5,'Practical Metal Keyboard','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',15000,'http://loremflickr.com/600/600/cats?70008',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(6,'Awesome Fresh Sausages','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',14000,'http://loremflickr.com/600/600/cats?51213',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(7,'Fantastic Soft Towels','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',13500,'http://loremflickr.com/600/600/cats?75755',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(8,'Tasty Soft Soap','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',13000,'http://loremflickr.com/600/600/cats?24698',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(9,'Handcrafted Plastic Keyboard','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',20000,'http://loremflickr.com/600/600/cats?6071',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(10,'Sleek Concrete Cheese','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',16000,'http://loremflickr.com/600/600/cats?59921',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(11,'Gorgeous Wooden Chair','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',14500,'http://loremflickr.com/600/600/cats?34213',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(12,'Awesome Cotton Pants','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',19000,'http://loremflickr.com/600/600/cats?52210',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(13,'Handcrafted Rubber Shirt','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',7000,'http://loremflickr.com/600/600/cats?69977',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(14,'Practical Fresh Tuna','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',9500,'http://loremflickr.com/600/600/cats?9267',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(15,'Awesome Wooden Table','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',5500,'http://loremflickr.com/600/600/cats?29371',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(16,'Incredible Steel Pants','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',17000,'http://loremflickr.com/600/600/cats?54104',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(17,'Gorgeous Soft Bacon','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',9500,'http://loremflickr.com/600/600/cats?41546',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(18,'Handmade Fresh Car','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',5500,'http://loremflickr.com/600/600/cats?61215',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(19,'Tasty Granite Keyboard','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',19500,'http://loremflickr.com/600/600/cats?19490',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(20,'Licensed Frozen Chips','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',20000,'http://loremflickr.com/600/600/cats?96955',1,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(21,'Awesome Frozen Car','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',13000,'http://loremflickr.com/600/600/cats?1421',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(22,'Awesome Plastic Shirt','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',19000,'http://loremflickr.com/600/600/cats?49781',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(23,'Awesome Cotton Sausages','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',5500,'http://loremflickr.com/600/600/cats?80477',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(24,'Awesome Metal Chair','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',10500,'http://loremflickr.com/600/600/cats?11221',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(25,'Handcrafted Plastic Bike','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',17500,'http://loremflickr.com/600/600/cats?30709',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(26,'Licensed Fresh Keyboard','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',10000,'http://loremflickr.com/600/600/cats?24834',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(27,'Gorgeous Cotton Gloves','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',14500,'http://loremflickr.com/600/600/cats?20613',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(28,'Fantastic Wooden Car','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',13000,'http://loremflickr.com/600/600/cats?93162',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(29,'Handcrafted Steel Salad','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',18000,'http://loremflickr.com/600/600/cats?58418',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(30,'Intelligent Frozen Soap','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',17500,'http://loremflickr.com/600/600/cats?54569',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(31,'Practical Metal Sausages','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',19500,'http://loremflickr.com/600/600/cats?2348',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(32,'Intelligent Plastic Towels','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',13000,'http://loremflickr.com/600/600/cats?91921',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(33,'Tasty Concrete Ball','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',7500,'http://loremflickr.com/600/600/cats?48244',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(34,'Handmade Steel Soap','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',18500,'http://loremflickr.com/600/600/cats?21672',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(35,'Small Metal Salad','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',6000,'http://loremflickr.com/600/600/cats?30373',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(36,'Ergonomic Plastic Salad','The Football Is Good For Training And Recreational Purposes',7500,'http://loremflickr.com/600/600/cats?31199',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(37,'Gorgeous Rubber Shoes','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',12500,'http://loremflickr.com/600/600/cats?54904',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(38,'Sleek Fresh Hat','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',17000,'http://loremflickr.com/600/600/cats?6059',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(39,'Small Metal Shirt','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',16500,'http://loremflickr.com/600/600/cats?51267',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(40,'Ergonomic Cotton Cheese','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',6500,'http://loremflickr.com/600/600/cats?18554',2,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(41,'Incredible Fresh Mouse','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',17500,'http://loremflickr.com/600/600/cats?19073',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(42,'Fantastic Plastic Pizza','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',16000,'http://loremflickr.com/600/600/cats?16032',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(43,'Handmade Concrete Shoes','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',6000,'http://loremflickr.com/600/600/cats?30460',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(44,'Incredible Metal Keyboard','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',13000,'http://loremflickr.com/600/600/cats?65340',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(45,'Handcrafted Wooden Car','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',7000,'http://loremflickr.com/600/600/cats?36688',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(46,'Small Concrete Sausages','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',8000,'http://loremflickr.com/600/600/cats?4455',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(47,'Sleek Wooden Mouse','The Football Is Good For Training And Recreational Purposes',6500,'http://loremflickr.com/600/600/cats?16736',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(48,'Practical Frozen Computer','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',7500,'http://loremflickr.com/600/600/cats?18005',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(49,'Generic Cotton Fish','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',6000,'http://loremflickr.com/600/600/cats?46499',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(50,'Small Frozen Ball','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',11500,'http://loremflickr.com/600/600/cats?12857',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(51,'Licensed Cotton Shoes','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',9000,'http://loremflickr.com/600/600/cats?60225',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(52,'Awesome Steel Bike','The Football Is Good For Training And Recreational Purposes',13500,'http://loremflickr.com/600/600/cats?59980',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(53,'Licensed Metal Soap','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',5000,'http://loremflickr.com/600/600/cats?67689',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(54,'Licensed Concrete Pizza','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',19000,'http://loremflickr.com/600/600/cats?64547',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(55,'Sleek Soft Towels','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',8500,'http://loremflickr.com/600/600/cats?30777',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(56,'Ergonomic Fresh Computer','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',18500,'http://loremflickr.com/600/600/cats?94393',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(57,'Sleek Soft Soap','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',19000,'http://loremflickr.com/600/600/cats?84721',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(58,'Licensed Wooden Car','The Football Is Good For Training And Recreational Purposes',7500,'http://loremflickr.com/600/600/cats?1473',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(59,'Sleek Soft Pizza','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',16500,'http://loremflickr.com/600/600/cats?28177',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(60,'Unbranded Wooden Computer','The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',19000,'http://loremflickr.com/600/600/cats?72436',3,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(61,'Fantastic Soft Cheese','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',5000,'http://loremflickr.com/600/600/cats?2157',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(62,'Sleek Wooden Cheese','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',11000,'http://loremflickr.com/600/600/cats?22468',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(63,'Tasty Wooden Mouse','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',5000,'http://loremflickr.com/600/600/cats?26661',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(64,'Awesome Metal Sausages','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',18000,'http://loremflickr.com/600/600/cats?93211',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(65,'Refined Concrete Ball','The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',9000,'http://loremflickr.com/600/600/cats?52948',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(66,'Rustic Plastic Keyboard','The Football Is Good For Training And Recreational Purposes',17500,'http://loremflickr.com/600/600/cats?17499',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(67,'Incredible Metal Chicken','The Football Is Good For Training And Recreational Purposes',5500,'http://loremflickr.com/600/600/cats?1157',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(68,'Fantastic Cotton Keyboard','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',9500,'http://loremflickr.com/600/600/cats?14706',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(69,'Unbranded Rubber Shoes','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',6500,'http://loremflickr.com/600/600/cats?80174',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(70,'Tasty Fresh Hat','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',16000,'http://loremflickr.com/600/600/cats?44681',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(71,'Handmade Cotton Hat','New range of formal shirts are designed keeping you in mind. With fits and styling that will make you stand apart',15000,'http://loremflickr.com/600/600/cats?32869',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(72,'Fantastic Rubber Gloves','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',15000,'http://loremflickr.com/600/600/cats?40023',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(73,'Handmade Metal Hat','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',12000,'http://loremflickr.com/600/600/cats?11339',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(74,'Gorgeous Frozen Gloves','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',17500,'http://loremflickr.com/600/600/cats?87561',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(75,'Small Cotton Computer','The Football Is Good For Training And Recreational Purposes',8000,'http://loremflickr.com/600/600/cats?1681',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(76,'Practical Plastic Soap','The Football Is Good For Training And Recreational Purposes',19000,'http://loremflickr.com/600/600/cats?81407',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(77,'Handmade Plastic Gloves','The automobile layout consists of a front-engine design, with transaxle-type transmissions mounted at the rear of the engine and four wheel drive',20000,'http://loremflickr.com/600/600/cats?8081',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(78,'Fantastic Cotton Gloves','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',18500,'http://loremflickr.com/600/600/cats?31722',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(79,'Practical Cotton Car','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',19500,'http://loremflickr.com/600/600/cats?9321',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(80,'Incredible Cotton Bike','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',20000,'http://loremflickr.com/600/600/cats?80750',4,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(81,'Practical Cotton Salad','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',7000,'http://loremflickr.com/600/600/cats?91351',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(82,'Ergonomic Granite Ball','The Football Is Good For Training And Recreational Purposes',7000,'http://loremflickr.com/600/600/cats?41829',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(83,'Licensed Soft Cheese','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',11500,'http://loremflickr.com/600/600/cats?38090',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(84,'Unbranded Frozen Fish','The Football Is Good For Training And Recreational Purposes',15500,'http://loremflickr.com/600/600/cats?73626',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(85,'Fantastic Frozen Chicken','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',7000,'http://loremflickr.com/600/600/cats?13217',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(86,'Practical Soft Shoes','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',17000,'http://loremflickr.com/600/600/cats?81570',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(87,'Ergonomic Plastic Pants','The beautiful range of Apple Naturalé that has an exciting mix of natural ingredients. With the Goodness of 100% Natural Ingredients',12500,'http://loremflickr.com/600/600/cats?57204',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(88,'Practical Steel Bacon','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',20000,'http://loremflickr.com/600/600/cats?81871',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(89,'Fantastic Metal Chair','The Nagasaki Lander is the trademarked name of several series of Nagasaki sport bikes, that started with the 1984 ABC800J',18500,'http://loremflickr.com/600/600/cats?56841',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(90,'Ergonomic Fresh Pizza','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',6500,'http://loremflickr.com/600/600/cats?4099',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(91,'Awesome Frozen Cheese','The Apollotech B340 is an affordable wireless mouse with reliable connectivity, 12 months battery life and modern design',17000,'http://loremflickr.com/600/600/cats?22093',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(92,'Ergonomic Granite Keyboard','New ABC 13 9370, 13.3, 5th Gen CoreA5-8250U, 8GB RAM, 256GB SSD, power UHD Graphics, OS 10 Home, OS Office A & J 2016',12500,'http://loremflickr.com/600/600/cats?65676',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(93,'Gorgeous Cotton Cheese','The Football Is Good For Training And Recreational Purposes',19000,'http://loremflickr.com/600/600/cats?58829',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(94,'Sleek Concrete Mouse','Carbonite web goalkeeper gloves are ergonomically designed to give easy fit',15500,'http://loremflickr.com/600/600/cats?12608',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(95,'Tasty Cotton Cheese','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',18000,'http://loremflickr.com/600/600/cats?52473',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(96,'Practical Metal Chair','Andy shoes are designed to keeping in mind durability as well as trends, the most stylish range of shoes & sandals',17000,'http://loremflickr.com/600/600/cats?35597',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(97,'Licensed Concrete Gloves','Boston\'s most advanced compression wear technology increases muscle oxygenation, stabilizes active muscles',18500,'http://loremflickr.com/600/600/cats?26878',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(98,'Tasty Frozen Salad','The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',8500,'http://loremflickr.com/600/600/cats?25722',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(99,'Unbranded Plastic Car','Ergonomic executive chair upholstered in bonded black leather and PVC padded seat and back for all-day comfort and support',7000,'http://loremflickr.com/600/600/cats?13093',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(100,'Awesome Fresh Tuna','The slim & simple Maple Gaming Keyboard from Dev Byte comes with a sleek body and 7- Color RGB LED Back-lighting for smart functionality',10500,'http://loremflickr.com/600/600/cats?59997',5,'2022-04-03 07:57:04','2022-04-03 07:57:04',NULL);
/*!40000 ALTER TABLE `Products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Promos`
--

DROP TABLE IF EXISTS `Promos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Promos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `merchantId` int(11) DEFAULT NULL,
  `productId` int(11) DEFAULT NULL,
  `matrix` varchar(100) DEFAULT NULL,
  `rules` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`rules`)),
  `description` text DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `merchantId` (`merchantId`),
  KEY `productId` (`productId`),
  CONSTRAINT `Promos_ibfk_1` FOREIGN KEY (`merchantId`) REFERENCES `Merchants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Promos_ibfk_2` FOREIGN KEY (`productId`) REFERENCES `Products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Promos`
--

LOCK TABLES `Promos` WRITE;
/*!40000 ALTER TABLE `Promos` DISABLE KEYS */;
INSERT INTO `Promos` VALUES
(1,1,1,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":1}','Beli 3 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(2,3,59,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":1}','Beli 3 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(3,3,42,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(4,5,88,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":1}','Beli 3 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(5,2,38,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(6,2,35,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":1}','Beli 9 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(7,4,73,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":2}','Beli 9 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(8,5,85,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":1}','Beli 5 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(9,2,21,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(10,1,8,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":2}','Beli 9 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(11,5,86,'Beli {buy} Gratis {free}','{\"buy\":4,\"free\":2}','Beli 4 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(12,3,49,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(13,1,10,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":2}','Beli 8 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(14,4,76,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":2}','Beli 7 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(15,5,92,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":2}','Beli 8 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(16,5,99,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":1}','Beli 5 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(17,3,44,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":1}','Beli 9 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(18,4,64,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(19,2,40,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(20,1,19,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":2}','Beli 3 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(21,4,78,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(22,4,61,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(23,4,66,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":1}','Beli 6 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(24,3,43,'Beli {buy} Gratis {free}','{\"buy\":4,\"free\":2}','Beli 4 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(25,2,33,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":2}','Beli 5 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(26,4,69,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":2}','Beli 7 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(27,1,15,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":1}','Beli 6 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(28,4,75,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":1}','Beli 3 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(29,1,17,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(30,5,91,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":2}','Beli 6 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(31,4,80,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":1}','Beli 6 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(32,4,67,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":2}','Beli 3 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(33,5,93,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":2}','Beli 8 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(34,5,97,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(35,4,71,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(36,3,41,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":2}','Beli 3 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(37,5,100,'Beli {buy} Gratis {free}','{\"buy\":4,\"free\":2}','Beli 4 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(38,5,89,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":1}','Beli 8 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(39,3,58,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":1}','Beli 6 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(40,1,6,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(41,5,82,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(42,2,29,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":1}','Beli 5 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(43,1,14,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":2}','Beli 9 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(44,2,23,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":1}','Beli 8 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(45,2,25,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(46,2,31,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":2}','Beli 6 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(47,3,45,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":2}','Beli 5 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(48,1,12,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":1}','Beli 10 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(49,2,37,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(50,5,98,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":2}','Beli 5 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(51,3,60,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":2}','Beli 6 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(52,2,27,'Beli {buy} Gratis {free}','{\"buy\":9,\"free\":1}','Beli 9 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(53,4,68,'Beli {buy} Gratis {free}','{\"buy\":10,\"free\":2}','Beli 10 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(54,4,63,'Beli {buy} Gratis {free}','{\"buy\":5,\"free\":1}','Beli 5 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(55,2,24,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":2}','Beli 6 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(56,3,52,'Beli {buy} Gratis {free}','{\"buy\":6,\"free\":1}','Beli 6 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(57,2,26,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":1}','Beli 8 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(58,2,30,'Beli {buy} Gratis {free}','{\"buy\":8,\"free\":1}','Beli 8 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(59,1,16,'Beli {buy} Gratis {free}','{\"buy\":7,\"free\":1}','Beli 7 Gratis 1','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(60,3,56,'Beli {buy} Gratis {free}','{\"buy\":3,\"free\":2}','Beli 3 Gratis 2','Active','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL);
/*!40000 ALTER TABLE `Promos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SequelizeMeta`
--

DROP TABLE IF EXISTS `SequelizeMeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SequelizeMeta` (
  `name` varchar(255) COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SequelizeMeta`
--

LOCK TABLES `SequelizeMeta` WRITE;
/*!40000 ALTER TABLE `SequelizeMeta` DISABLE KEYS */;
INSERT INTO `SequelizeMeta` VALUES
('20220330165604-create-merchant.js'),
('20220330170951-create-product.js'),
('20220330171743-create-promo.js'),
('20220330172319-create-user.js'),
('20220330173359-create-cart.js'),
('20220330174044-create-order.js'),
('20220330175016-create-order-item.js');
/*!40000 ALTER TABLE `SequelizeMeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT current_timestamp(),
  `updatedAt` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deletedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES
(1,'Sugeng Winanjuar','winanjuar@mail.com','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(2,'Gilda Yulianti','eldridge.trantow@gmail.co.id','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(3,'Yunita Haryanti','adrain34@yahoo.co.id','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(4,'Gibran Satria','hans_lueilwitz@gmail.co.id','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(5,'Rika Susanti','cleta5@yahoo.com','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(6,'Luwes Prasasta','giovani98@gmail.co.id','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(7,'Umar Haikal','gregorio.durgan@gmail.com','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(8,'Salimah Pertiwi','santos_feeney@gmail.com','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(9,'Luwar Wahyudin','abigale.waelchi@gmail.com','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL),
(10,'Gamani Pratama','hank_muller@gmail.co.id','$2a$10$sBv.mY5OE97MZCbuHZm9XuLEUwHCGLIqVesRezpJG4htKuvaUqFFO','2022-04-03 07:57:04','2022-04-03 07:57:04',NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-03 15:01:23
