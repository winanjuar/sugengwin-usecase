"use strict";
const { faker } = require("@faker-js/faker");
const hashHelper = require("../helpers/hash-helper");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const password = await hashHelper.generateHashPassword("letmein");
    faker.locale = "id_ID";
    const users = [];

    const defaultUser = {
      id: 1,
      name: "Sugeng Winanjuar",
      email: "winanjuar@mail.com",
      password,
    };
    users.push(defaultUser);

    for (let i = 2; i <= 10; i++) {
      const user = {
        id: i,
        name: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        password,
      };
      users.push(user);
    }
    await queryInterface.bulkInsert("Users", users, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
