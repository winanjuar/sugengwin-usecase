"use strict";
const { faker } = require("@faker-js/faker");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    faker.locale = "id_ID";
    const merchants = [];
    for (let i = 1; i <= 5; i++) {
      const merchant = {
        id: i,
        name: faker.internet.userName().toLowerCase() + "_official",
      };
      merchants.push(merchant);
    }
    await queryInterface.bulkInsert("Merchants", merchants, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
