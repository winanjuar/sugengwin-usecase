"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    const carts = [
      {
        id: 1,
        userId: 1,
        merchantId: 1,
        productId: 1,
        promoId: 1,
        price: 6000,
        quantity: 8,
        fixBuy: 6,
        free: 2,
        fixFree: 2,
        fixQuantity: 6,
        bruto: 48000,
        discount: 12000,
        netto: 36000,
      },
    ];

    await queryInterface.bulkInsert("Carts", carts, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
