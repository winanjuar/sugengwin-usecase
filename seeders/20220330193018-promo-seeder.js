"use strict";
const { faker } = require("@faker-js/faker");
const pairMerchantProducts = require("../repositories/pair-merchant-product");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    faker.locale = "id_ID";
    const promos = [];
    const pairs = pairMerchantProducts(20);
    let index = 0;
    let defaultPair = pairs[index];
    let promo = {
      id: 1,
      merchantId: defaultPair.merchant,
      productId: defaultPair.product,
      matrix: "Beli {buy} Gratis {free}",
      rules: JSON.stringify({
        buy: 3,
        free: 1,
      }),
      description: `Beli 3 Gratis 1`,
      status: "Active",
    };
    pairs.splice(index, 1);
    promos.push(promo);

    for (let i = 2; i <= 60; i++) {
      let pair;
      while (pair === undefined) {
        index = faker.datatype.number({ min: 2, max: 100 });
        pair = pairs[index];
      }
      const buy = faker.datatype.number({ min: 3, max: 10 });
      const free = faker.datatype.number({ min: 1, max: 2 });
      promo = {
        id: i,
        merchantId: pair.merchant,
        productId: pair.product,
        matrix: "Beli {buy} Gratis {free}",
        rules: JSON.stringify({
          buy,
          free,
        }),
        description: `Beli ${buy} Gratis ${free}`,
        status: "Active",
      };
      pairs.splice(index, 1);
      promos.push(promo);
    }
    await queryInterface.bulkInsert("Promos", promos, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
