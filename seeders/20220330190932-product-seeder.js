"use strict";
const { faker } = require("@faker-js/faker");
const pairMerchantProducts = require("../repositories/pair-merchant-product");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    faker.locale = "id_ID";
    const products = [];
    const pairs = pairMerchantProducts(20);

    for (let i = 1; i <= 100; i++) {
      const pairMerchantProduct = pairs.find((pair) => pair.product === i);
      const product = {
        id: i,
        name: faker.commerce.productName(),
        description: faker.commerce.productDescription(),
        price:
          i === 1 || i === 2
            ? 6000
            : faker.datatype.number({ min: 10, max: 40 }) * 500,
        image: faker.image.cats(600, 600, true),
        merchantId: pairMerchantProduct.merchant,
      };
      products.push(product);
    }
    await queryInterface.bulkInsert("Products", products, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
