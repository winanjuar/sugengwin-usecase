"use strict";
const crypto = require("crypto");
const { faker } = require("@faker-js/faker");
const { moment, randInt } = require("../helpers/general-helper");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    faker.locale = "id_ID";
    const orderId = crypto.randomUUID();
    const orderItems = [
      {
        id: crypto.randomUUID(),
        orderId,
        infoCartId: 1,
        merchantId: 1,
        productId: 1,
        promoId: 1,
        price: 6000,
        quantity: 4,
        fixBuy: 3,
        free: 1,
        fixFree: 1,
        fixQuantity: 4,
        bruto: 24000,
        discount: 6000,
        netto: 18000,
      },
      {
        id: crypto.randomUUID(),
        orderId,
        infoCartId: 2,
        merchantId: 1,
        productId: 2,
        promoId: null,
        price: 6000,
        quantity: 2,
        fixBuy: 2,
        free: 0,
        fixFree: 0,
        fixQuantity: 2,
        bruto: 12000,
        discount: 0,
        netto: 12000,
      },
    ];

    const grandBruto = orderItems.reduce(
      (total, orderItem) => total + orderItem.bruto,
      0
    );

    const grandDiscount = orderItems.reduce(
      (total, orderItem) => total + orderItem.discount,
      0
    );

    const grandNetto = grandBruto - grandDiscount;

    const orders = [
      {
        id: orderId,
        userId: 1,
        orderNumber: `ORD${moment()}${randInt()}`,
        orderTime: faker.date.past(),
        grandBruto,
        grandDiscount,
        grandNetto,
      },
    ];

    await queryInterface.bulkInsert("Orders", orders, {});
    await queryInterface.bulkInsert("OrderItems", orderItems, {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
