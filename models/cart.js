"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Cart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.User, {
        foreignKey: "userId",
        as: "user",
      });

      this.belongsTo(models.Merchant, {
        foreignKey: "merchantId",
        as: "merchant",
      });

      this.belongsTo(models.Product, {
        foreignKey: "productId",
        as: "product",
      });

      this.belongsTo(models.Promo, {
        foreignKey: "promoId",
        as: "promo",
      });
    }
  }
  Cart.init(
    {
      userId: DataTypes.INTEGER,
      merchantId: DataTypes.INTEGER,
      productId: DataTypes.INTEGER,
      promoId: DataTypes.INTEGER,
      price: DataTypes.DOUBLE,
      quantity: DataTypes.INTEGER,
      fixBuy: DataTypes.INTEGER,
      free: DataTypes.INTEGER,
      fixFree: DataTypes.INTEGER,
      fixQuantity: DataTypes.INTEGER,
      bruto: DataTypes.DOUBLE,
      discount: DataTypes.DOUBLE,
      netto: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "Cart",
      timestamps: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      },
    }
  );
  return Cart;
};
