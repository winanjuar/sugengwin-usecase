"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Promo extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Promo.init(
    {
      merchantId: DataTypes.INTEGER,
      productId: DataTypes.INTEGER,
      matrix: DataTypes.STRING,
      rules: DataTypes.JSON,
      description: DataTypes.TEXT,
      status: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Promo",
      timestamps: true,
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["matrix", "createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Promo;
};
