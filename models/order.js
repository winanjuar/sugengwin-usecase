"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.OrderItem, {
        foreignKey: "orderId",
        as: "orderItems",
      });

      this.belongsTo(models.User, {
        foreignKey: "userId",
        as: "user",
      });
    }
  }
  Order.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        type: DataTypes.UUID,
      },
      userId: DataTypes.INTEGER,
      orderNumber: DataTypes.STRING,
      orderTime: DataTypes.DATE,
      grandBruto: DataTypes.DOUBLE,
      grandDiscount: DataTypes.DOUBLE,
      grandNetto: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "Order",
      timestamps: true,
      paranoid: true,
      defaultScope: {
        attributes: {
          exclude: ["createdAt", "updatedAt", "deletedAt"],
        },
      },
    }
  );
  return Order;
};
