const pairMerchantProducts = (gap) => {
  const pairs = [];
  for (let i = 1; i <= 5; i++) {
    for (let j = 1; j <= gap; j++) {
      const pair = {
        merchant: i,
        product: (i - 1) * gap + j,
      };
      pairs.push(pair);
    }
  }
  return pairs;
};

module.exports = pairMerchantProducts;
