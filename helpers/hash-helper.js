const bcrypt = require("bcryptjs");
require("dotenv").config();

const generateHashPassword = async (password) => {
  const salt = await bcrypt.genSalt(parseInt(process.env.SALT_ROUND));
  return await bcrypt.hash(password, salt);
};

const compareHashedPassword = async (password, hashedPassword) => {
  return await bcrypt.compare(password, hashedPassword);
};

module.exports = {
  generateHashPassword,
  compareHashedPassword,
};
