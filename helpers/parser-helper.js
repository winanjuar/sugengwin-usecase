const errorValidationParser = (error) =>
  error.details.map((detail) => detail.message);

const quantityParser = (quantity, promo = {}) => {
  /* 
  Test case, quantity 4, promo rules buy 3 get 1 free
  will return
  fixBuy: 3 => 1 * 3 + 0
  free: 1,
  fixFree: 1,
  fixQuantity: 4, 

  Test case, quantity 6, promo rules buy 3 get 1 free
  will return
  fixBuy: 5 => 1 * 3 + 2
  free: 1,
  fixFree: 1,
  fixQuantity: 6, 

  Test case, quantity 7, promo rules buy 3 get 1 free
  will return
  fixBuy: 6 => 1 * 3 + 3
  free: 1,
  fixFree: 2,
  fixQuantity: 8, 
  */

  if (promo) {
    const package = promo.rules.buy + promo.rules.free;
    const packageBuy = Math.floor(quantity / package);
    const buyOutPackage = quantity % package;
    const correction = buyOutPackage === promo.rules.buy ? promo.rules.free : 0;
    const fixBuy = packageBuy * promo.rules.buy + buyOutPackage;
    const free = packageBuy * promo.rules.free;
    const fixFree = free + correction;
    const fixQuantity = fixBuy + fixFree;
    return {
      fixBuy,
      free,
      fixFree,
      fixQuantity,
    };
  } else {
    return {
      fixBuy: quantity,
      free: 0,
      fixFree: 0,
      fixQuantity: quantity,
    };
  }
};

module.exports = {
  errorValidationParser,
  quantityParser,
};
