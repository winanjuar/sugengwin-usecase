const Joi = require("joi");

const addToCart = (data) => {
  const schema = Joi.object({
    userId: Joi.number().integer().min(1).required(),
    productId: Joi.number().integer().min(1).required(),
    quantity: Joi.number().integer().min(1).required(),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

const checkout = (data) => {
  const schema = Joi.object({
    userId: Joi.number().integer().min(1).required(),
    cartIds: Joi.array().items(Joi.number().integer().required()),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

const checkUserId = (data) => {
  const schema = Joi.object({
    userId: Joi.number().integer().min(1).required(),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

const getProductList = (data) => {
  const schema = Joi.object({
    page: Joi.number().integer().min(1).optional(),
    sortBy: Joi.number().valid("name", "price").optional(),
    sortDir: Joi.valid("ASC", "DESC").optional(),
    limit: Joi.number().valid(5, 10, 15, 20, 25, 30, 100).optional(),
    merchantId: Joi.number().integer().min(1).optional(),
    search: Joi.string().optional(),
    priceFrom: Joi.number().integer().min(0).optional(),
    priceTo: Joi.number()
      .integer()
      .min(Joi.ref("priceFrom"))
      .when("priceFrom", {
        is: Joi.exist(),
        then: Joi.required(),
        otherwise: Joi.optional(),
      }),
    hasPromo: Joi.string().valid("yes", "no").optional(),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

const getOrderDetail = (data) => {
  const schema = Joi.object({
    orderId: Joi.string().guid().required(),
    userId: Joi.number().integer().min(1).required(),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

const postLogin = (data) => {
  const schema = Joi.object({
    email: Joi.string().email().trim().lowercase().required(),
    password: Joi.string().min(3).max(15).required(),
  }).options({ abortEarly: false });
  return schema.validate(data);
};

module.exports = {
  addToCart,
  checkout,
  checkUserId,
  getOrderDetail,
  getProductList,
  postLogin,
};
