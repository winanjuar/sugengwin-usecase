const JWT = require("jsonwebtoken");

const generateToken = (currentUser) =>
  JWT.sign({ currentUser }, process.env.SECRET_KEY, { expiresIn: "60m" });

const verifyToken = (token) => JWT.verify(token, process.env.SECRET_KEY);

module.exports = {
  generateToken,
  verifyToken,
};
