const momentTz = require("moment-timezone");

const isEmpty = (obj) => Object.keys(obj).length === 0;

const moment = (format = "YYMMDDHHmm") => {
  const mtz = momentTz.tz.setDefault("Asia/Jakarta");
  return mtz().format(format);
};

const randInt = () => Math.floor(Math.random() * (99 - 10) + 10);

const isAllMember = (sources, targets) =>
  sources.every((element) => targets.includes(element));

module.exports = { isEmpty, moment, randInt, isAllMember };
