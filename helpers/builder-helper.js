const { quantityParser } = require("./parser-helper");

const cartBuilder = (dataInput) => {
  const parsedQuantity = quantityParser(
    dataInput.quantity,
    dataInput.product.promo
  );
  const bruto = parsedQuantity.fixQuantity * dataInput.product.price;
  const discount = parsedQuantity.fixFree * dataInput.product.price;
  const netto = bruto - discount;
  return {
    userId: dataInput.userId,
    merchantId: dataInput.product.merchantId,
    productId: dataInput.productId,
    promoId:
      dataInput.product.promo && dataInput.product.promo.id
        ? dataInput.product.promo.id
        : null,
    quantity: dataInput.quantity,
    price: dataInput.product.price,
    ...parsedQuantity,
    bruto,
    discount,
    netto,
  };
};

module.exports = {
  cartBuilder,
};
