"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("OrderItems", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      orderId: {
        type: Sequelize.UUID,
        references: {
          model: "Orders",
          key: "id",
        },
        onDelete: "Cascade",
        onUpdate: "Cascade",
      },
      infoCartId: Sequelize.INTEGER,
      merchantId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Merchants",
          key: "id",
        },
        onDelete: "Cascade",
        onUpdate: "Cascade",
      },
      productId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Products",
          key: "id",
        },
        onDelete: "Cascade",
        onUpdate: "Cascade",
      },
      promoId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: "Promos",
          key: "id",
        },
        onDelete: "Cascade",
        onUpdate: "Cascade",
      },
      price: Sequelize.DOUBLE,
      quantity: Sequelize.INTEGER,
      fixBuy: Sequelize.INTEGER,
      free: Sequelize.INTEGER,
      fixFree: Sequelize.INTEGER,
      fixQuantity: Sequelize.INTEGER,
      bruto: Sequelize.DOUBLE,
      discount: Sequelize.DOUBLE,
      netto: Sequelize.DOUBLE,
      createdAt: {
        allowNull: false,
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      updatedAt: {
        allowNull: false,
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal(
          "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
        ),
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("OrderItems");
  },
};
