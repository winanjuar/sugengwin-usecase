"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Carts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      userId: Sequelize.INTEGER,
      merchantId: Sequelize.INTEGER,
      productId: Sequelize.INTEGER,
      promoId: Sequelize.INTEGER,
      price: Sequelize.DOUBLE,
      quantity: Sequelize.INTEGER,
      fixBuy: Sequelize.INTEGER,
      free: Sequelize.INTEGER,
      fixFree: Sequelize.INTEGER,
      fixQuantity: Sequelize.INTEGER,
      bruto: Sequelize.DOUBLE,
      discount: Sequelize.DOUBLE,
      netto: Sequelize.DOUBLE,
      createdAt: {
        allowNull: false,
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP"),
      },
      updatedAt: {
        allowNull: false,
        type: "TIMESTAMP",
        defaultValue: Sequelize.literal(
          "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
        ),
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Carts");
  },
};
