var express = require("express");
var router = express.Router();
const { v_01 } = require("../helpers/version-helper");

const CartController = require("../controllers/cart-controller");

const passport = require("../middlewares/user-auth-middleware");
router.use(passport.initialize());
router.use(passport.authenticate("jwt", { session: false }));

router.get(v_01 + "/", CartController.list);
router.post(v_01 + "/", CartController.add);

module.exports = router;
