var express = require("express");
var router = express.Router();
const { v_01 } = require("../helpers/version-helper");

const UserController = require("../controllers/user-controller");

router.post(v_01 + "/login", UserController.login);

module.exports = router;
