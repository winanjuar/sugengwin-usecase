var express = require("express");
var router = express.Router();
const { v_01 } = require("../helpers/version-helper");

const ProductController = require("../controllers/product-controller");

router.get(v_01 + "/", ProductController.list);

module.exports = router;
