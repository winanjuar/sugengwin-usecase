var express = require("express");
var router = express.Router();
const { v_01 } = require("../helpers/version-helper");

const OrderController = require("../controllers/order-controller");

const passport = require("../middlewares/user-auth-middleware");
router.use(passport.initialize());
router.use(passport.authenticate("jwt", { session: false }));

router.get(v_01 + "/", OrderController.list);
router.get(v_01 + "/:id", OrderController.detail);
router.post(v_01 + "/checkout", OrderController.checkout);

module.exports = router;
