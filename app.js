const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const helmet = require("helmet");
const hpp = require("hpp");
require("dotenv").config();

const app = express();
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

// for security purposes, follow OWASP
app.use(helmet.hidePoweredBy());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      "default-src": ["'self'"],
      "img-src": ["loremflickr.com"],
    },
  })
);
app.use(helmet.hsts({ maxAge: 7776000000 }));
app.use(helmet.frameguard("SAMEORIGIN"));
app.use(helmet.xssFilter({ setOnOldIE: true }));
app.use(helmet.noSniff());

app.use(hpp());

const indexRouter = require("./routes/index");
const cartRouter = require("./routes/cart-router");
const orderRouter = require("./routes/order-router");
const productRouter = require("./routes/product-router");
const userRouter = require("./routes/user-router");

app.use("/", indexRouter);
app.use("/cart", cartRouter);
app.use("/order", orderRouter);
app.use("/product", productRouter);
app.use("/user", userRouter);

module.exports = app;
