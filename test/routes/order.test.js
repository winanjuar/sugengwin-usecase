const _ = require("lodash");
const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");

const app = require("../../app");
const TestHelper = require("../helpers/test-helper");
const ValidationHelper = require("../../helpers/validation-helper");
const GeneralHelper = require("../../helpers/general-helper");
const { Order, OrderItem, Cart } = require("../../models");
const MockListOrder = require("../mocks/list-order.json");
const MockDetailOrderWithInclude = require("../mocks/detail-order-with-include.json");
const MockDetailOrderOnly = require("../mocks/detail-order-only.json");
const MockListCart = require("../mocks/list-cart.json");

chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

let mockListOrder;
let mockDetailOrderWithInclude;
let mockDetailOrderOnly;
let mockListCart;

let stubValidationHelper;
let stubGeneralHelper;
let stubOrderFindAll;
let stubOrderFindOne;
let stubOrderCreate;
let stubOrderItemBulkCreate;
let stubCartFindAll;
let stubCartDestroy;

describe("Order Test", () => {
  describe("GET /order/v1", () => {
    beforeEach((done) => {
      mockListOrder = _.cloneDeep(MockListOrder);
      done();
    });

    afterEach((done) => {
      if (stubOrderFindAll) stubOrderFindAll.restore();
      if (stubValidationHelper) stubValidationHelper.restore();
      done();
    });

    it("positive case: it should responds with 200, show list order", (done) => {
      stubOrderFindAll = sinon.stub(Order, "findAll").callsFake(() => {
        return mockListOrder;
      });

      chai
        .request(app)
        .get("/order/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list transaction successfully"
          );
          response.body.should.have.property("data").and.to.be.an("array");
          response.body.data[0].should.have
            .property("orderNumber")
            .and.to.be.a("string");
          response.body.data[0].should.have.property("grandBruto");
          response.body.data[0].should.have.property("grandDiscount");
          response.body.data[0].should.have.property("grandNetto");
          done();
        });
    });

    it("positive case: it should responds with 200, but still does not have order", (done) => {
      stubOrderFindAll = sinon.stub(Order, "findAll").callsFake(() => {
        return [];
      });

      chai
        .request(app)
        .get("/order/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Sorry, your does not have transaction"
          );
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 401, unauthorized", (done) => {
      chai
        .request(app)
        .get("/order/v1")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.text).to.equal("Unauthorized");
          done();
        });
    });

    it("negative case: it should responds with 406, missing mandatory field", (done) => {
      stubValidationHelper = sinon
        .stub(ValidationHelper, "checkUserId")
        .returns({ value: {}, error: { details: ["missing user id"] } });

      chai
        .request(app)
        .get("/order/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      stubOrderFindAll = sinon.stub(Order, "findAll").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get("/order/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });

  describe("GET /order/v1/:id", () => {
    beforeEach((done) => {
      mockDetailOrderWithInclude = _.cloneDeep(MockDetailOrderWithInclude);
      done();
    });

    afterEach((done) => {
      if (stubOrderFindOne) stubOrderFindOne.restore();
      if (stubValidationHelper) stubValidationHelper.restore();
      done();
    });

    it("positive case: it should responds with 200, show list order", (done) => {
      stubOrderFindOne = sinon.stub(Order, "findOne").callsFake(() => {
        return mockDetailOrderWithInclude;
      });

      chai
        .request(app)
        .get("/order/v1/176ff831-ae42-4bab-ae4f-27ea4bcbeefe")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal("Get order successfully");
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have.property("user").and.to.be.a("object");
          response.body.data.should.have
            .property("orderItems")
            .and.to.be.an("array");
          response.body.data.orderItems[0].should.have.property("merchant");
          response.body.data.orderItems[0].should.have.property("product");
          response.body.data.orderItems[0].should.have.property("promo");
          done();
        });
    });

    it("positive case: it should responds with 200, but order not found", (done) => {
      stubOrderFindOne = sinon.stub(Order, "findOne").callsFake(() => {
        return {};
      });

      chai
        .request(app)
        .get("/order/v1/176ff831-ae42-4bab-ae4f-27ea4bcbeeff")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal("Sorry, order not found");
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 401, unauthorized", (done) => {
      chai
        .request(app)
        .get("/order/v1/176ff831-ae42-4bab-ae4f-27ea4bcbeefe")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.text).to.equal("Unauthorized");
          done();
        });
    });

    it("negative case: it should responds with 406, wrong format order id", (done) => {
      chai
        .request(app)
        .get("/order/v1/asal")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      stubOrderFindOne = sinon.stub(Order, "findOne").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get("/order/v1/176ff831-ae42-4bab-ae4f-27ea4bcbeefe")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });

  describe("POST /order/v1/checkout", () => {
    beforeEach((done) => {
      mockListCart = _.cloneDeep(MockListCart);
      mockDetailOrderOnly = _.cloneDeep(MockDetailOrderOnly);
      done();
    });

    afterEach((done) => {
      if (stubGeneralHelper) stubGeneralHelper.restore();
      if (stubCartFindAll) stubCartFindAll.restore();
      if (stubOrderCreate) stubOrderCreate.restore();
      if (stubOrderItemBulkCreate) stubOrderItemBulkCreate.restore();
      if (stubCartDestroy) stubCartDestroy.restore();
      done();
    });

    it("positive case: it should responds with 200, insert new product with promo to cart", (done) => {
      const dataPost = { cartIds: [8, 9] };

      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return mockListCart;
      });

      stubOrderCreate = sinon.stub(Order, "create").callsFake(() => {
        return mockDetailOrderOnly.order;
      });

      stubOrderItemBulkCreate = sinon
        .stub(OrderItem, "bulkCreate")
        .callsFake(() => {
          return mockDetailOrderOnly.orderItems;
        });

      stubCartDestroy = sinon.stub(Cart, "destroy").callsFake(() => {
        return;
      });

      chai
        .request(app)
        .post("/order/v1/checkout")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal("Checkout successfully");
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have
            .property("order")
            .and.to.be.a("object");
          response.body.data.should.have
            .property("orderItems")
            .and.to.be.an("array");
          done();
        });
    });

    it("negative case: it should responds with 401, failed due to any element to checkout outside cart", (done) => {
      const dataPost = { cartIds: [8, 9, 10] };

      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return mockListCart;
      });

      stubGeneralHelper = sinon
        .stub(GeneralHelper, "isAllMember")
        .returns(false);

      chai
        .request(app)
        .post("/order/v1/checkout")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.body.message).to.equal("Sorry, checkout failed");
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 401, unauthorized", (done) => {
      const dataPost = { cartIds: [8, 9] };

      chai
        .request(app)
        .post("/order/v1/checkout")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.text).to.equal("Unauthorized");
          done();
        });
    });

    it("negative case: it should responds with 406, wrong format input", (done) => {
      const dataPost = { cartIds: 8 };
      chai
        .request(app)
        .post("/order/v1/checkout")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      const dataPost = { cartIds: [8] };

      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post("/order/v1/checkout")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });
});
