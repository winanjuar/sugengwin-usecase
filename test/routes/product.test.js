const _ = require("lodash");
const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");

const app = require("../../app");
const Product = require("../../models").Product;
const MockListProduct = require("../mocks/list-product.json");

chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

let mockListProduct;

let stubProductCount;
let stubProductFindAll;

describe("Product Test", () => {
  describe("GET /product/v1", () => {
    beforeEach((done) => {
      mockListProduct = _.cloneDeep(MockListProduct);
      done();
    });

    afterEach((done) => {
      if (stubProductCount) stubProductCount.restore();
      if (stubProductFindAll) stubProductFindAll.restore();
      done();
    });

    it("positive case: it should responds with 200, show list product", (done) => {
      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return mockListProduct.length;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return mockListProduct;
      });

      chai
        .request(app)
        .get("/product/v1")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list product successfully"
          );
          done();
        });
    });

    it("positive case: it should responds with 200, show list product with filter name, price, and has promo", (done) => {
      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return mockListProduct.length;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return mockListProduct;
      });

      chai
        .request(app)
        .get(
          "/product/v1?search=dress&priceFrom=1000&priceTo=5000&hasPromo=yes"
        )
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list product successfully"
          );
          done();
        });
    });

    it("positive case: it should responds with 200, show list product has promo, in 2nd page with limit 5 items per page, sorted by name desc", (done) => {
      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return mockListProduct.length;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return mockListProduct;
      });

      chai
        .request(app)
        .get("/product/v1?hasPromo=yes&limit=5&page=2&sortBy=name&sortDir=DESC")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list product successfully"
          );
          done();
        });
    });

    it("positive case: it should responds with 200, show list product with filter merchant but does not have promo", (done) => {
      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return mockListProduct.length;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return mockListProduct;
      });

      chai
        .request(app)
        .get("/product/v1?merchantId=1&hasPromo=no")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list product successfully"
          );
          done();
        });
    });

    it("positive case: it should responds with 200, but return empty result", (done) => {
      mockListProduct = [];

      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return mockListProduct.length;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return mockListProduct;
      });

      chai
        .request(app)
        .get("/product/v1")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Sorry, your filter return nothing"
          );
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 406, missing mandatory field", (done) => {
      chai
        .request(app)
        .get("/product/v1?priceFrom=1000")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          response.body.message.should.be.an("array");
          done();
        });
    });

    it("negative case: it should responds with 406, wrong format field", (done) => {
      chai
        .request(app)
        .get("/product/v1?hasPromo=true")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          response.body.message.should.be.an("array");
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      stubProductCount = sinon.stub(Product, "count").callsFake(() => {
        return error;
      });

      stubProductFindAll = sinon.stub(Product, "findAll").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get("/product/v1")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });
});
