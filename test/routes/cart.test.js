const _ = require("lodash");
const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");

const app = require("../../app");
const TestHelper = require("../helpers/test-helper");
const ValidationHelper = require("../../helpers/validation-helper");
const { Cart, Product } = require("../../models");
const MockListCart = require("../mocks/list-cart.json");
const MockProductWithPromo = require("../mocks/product-with-promo.json");
const MockDetailCart = require("../mocks/detail-cart.json");

chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

let mockListCart;
let mockDetailCart;
let mockProductWithPromo;

let stubValidationHelper;
let stubProductFindByPK;
let stubCartFindAll;
let stubCartFindByPK;
let stubCartFindOne;
let stubCartCreate;
let stubCartUpdate;

describe("Cart Test", () => {
  describe("GET /cart/v1", () => {
    beforeEach((done) => {
      mockListCart = _.cloneDeep(MockListCart);
      done();
    });

    afterEach((done) => {
      if (stubCartFindAll) stubCartFindAll.restore();
      if (stubValidationHelper) stubValidationHelper.restore();
      done();
    });

    it("positive case: it should responds with 200, show list cart", (done) => {
      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return mockListCart;
      });

      chai
        .request(app)
        .get("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Get list product in cart successfully"
          );
          response.body.should.have.property("data").and.to.be.an("array");
          response.body.data[0].should.have.property("user");
          response.body.data[0].should.have.property("merchant");
          response.body.data[0].should.have.property("product");
          response.body.data[0].should.have.property("promo");
          done();
        });
    });

    it("positive case: it should responds with 200, but still empty cart", (done) => {
      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return [];
      });

      chai
        .request(app)
        .get("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Sorry, your cart still empty"
          );
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 401, unauthorized", (done) => {
      chai
        .request(app)
        .get("/cart/v1")
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.text).to.equal("Unauthorized");
          done();
        });
    });

    it("negative case: it should responds with 406, missing mandatory field", (done) => {
      stubValidationHelper = sinon
        .stub(ValidationHelper, "checkUserId")
        .returns({ value: {}, error: { details: ["missing user id"] } });

      chai
        .request(app)
        .get("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      stubCartFindAll = sinon.stub(Cart, "findAll").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .get("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });

  describe("POST /cart/v1", () => {
    beforeEach((done) => {
      mockProductWithPromo = _.cloneDeep(MockProductWithPromo);
      mockDetailCart = _.cloneDeep(MockDetailCart);
      done();
    });

    afterEach((done) => {
      if (stubProductFindByPK) stubProductFindByPK.restore();
      if (stubCartFindOne) stubCartFindOne.restore();
      if (stubCartFindByPK) stubCartFindByPK.restore();
      if (stubCartUpdate) stubCartUpdate.restore();
      if (stubCartCreate) stubCartCreate.restore();
      done();
    });

    it("positive case: it should responds with 200, insert new product with promo to cart", (done) => {
      const dataPost = {
        productId: 1,
        quantity: 5,
      };

      stubProductFindByPK = sinon.stub(Product, "findByPk").callsFake(() => {
        return mockProductWithPromo;
      });

      stubCartFindOne = sinon.stub(Cart, "findOne").callsFake(() => {
        return null;
      });

      stubCartCreate = sinon.stub(Cart, "create").callsFake(() => {
        return mockDetailCart;
      });

      chai
        .request(app)
        .post("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Add product to cart successfully"
          );
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have.property("cart").and.to.be.a("object");
          response.body.data.should.have
            .property("promo")
            .and.to.be.a("string");
          response.body.data.cart.should.have
            .property("fixBuy")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixFree")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixQuantity")
            .and.to.be.a("number");
          done();
        });
    });

    it("positive case: it should responds with 200, insert new product without promo to cart", (done) => {
      const dataPost = {
        productId: 2,
        quantity: 5,
      };

      mockProductWithPromo.promo = null;
      stubProductFindByPK = sinon.stub(Product, "findByPk").callsFake(() => {
        return mockProductWithPromo;
      });

      stubCartFindOne = sinon.stub(Cart, "findOne").callsFake(() => {
        return null;
      });

      mockDetailCart.free = 0;
      mockDetailCart.fixFree = 0;
      mockDetailCart.discount = 0;

      stubCartCreate = sinon.stub(Cart, "create").callsFake(() => {
        return mockDetailCart;
      });

      chai
        .request(app)
        .post("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Add product to cart successfully"
          );
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have.property("cart").and.to.be.a("object");
          response.body.data.should.have.property("promo").and.to.be.null;
          response.body.data.cart.should.have
            .property("fixBuy")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixFree")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixQuantity")
            .and.to.be.a("number");
          expect(response.body.data.cart.free).to.equal(0);
          expect(response.body.data.cart.fixFree).to.equal(0);
          expect(response.body.data.cart.discount).to.equal(0);
          done();
        });
    });

    it("positive case: it should responds with 200, update existing product with promo in cart because repick product", (done) => {
      const dataPost = {
        productId: 1,
        quantity: 1,
      };

      stubProductFindByPK = sinon.stub(Product, "findByPk").callsFake(() => {
        return mockProductWithPromo;
      });

      stubCartFindOne = sinon.stub(Cart, "findOne").callsFake(() => {
        return mockDetailCart;
      });

      const mockUpdatedCart = _.cloneDeep(mockDetailCart);
      mockUpdatedCart.quantity += dataPost.quantity; //biar beda aja

      stubCartUpdate = sinon.stub(Cart, "update").callsFake(() => {
        return [1];
      });

      stubCartFindByPK = sinon.stub(Cart, "findByPk").callsFake(() => {
        return mockUpdatedCart;
      });

      chai
        .request(app)
        .post("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal(
            "Update product in cart successfully"
          );
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have.property("cart").and.to.be.a("object");
          response.body.data.should.have
            .property("promo")
            .and.to.be.a("string");
          response.body.data.cart.should.have
            .property("fixBuy")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixFree")
            .and.to.be.a("number");
          response.body.data.cart.should.have
            .property("fixQuantity")
            .and.to.be.a("number");
          done();
        });
    });

    it("negative case: it should responds with 401, unauthorized", (done) => {
      const dataPost = {
        productId: 1,
        quantity: 1,
      };

      chai
        .request(app)
        .post("/cart/v1")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.text).to.equal("Unauthorized");
          done();
        });
    });

    it("negative case: it should responds with 406, missing mandatory field", (done) => {
      const dataPost = {
        productId: 1,
      };

      chai
        .request(app)
        .post("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      const dataPost = {
        productId: 1,
        quantity: 1,
      };

      stubProductFindByPK = sinon.stub(Product, "findByPk").callsFake(() => {
        return error;
      });

      chai
        .request(app)
        .post("/cart/v1")
        .set(TestHelper.getDefaultHeaders())
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });
});
