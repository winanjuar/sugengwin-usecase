const _ = require("lodash");
const chai = require("chai");
const chaiHttp = require("chai-http");
const sinon = require("sinon");

const app = require("../../app");
const User = require("../../models").User;
const HashHelper = require("../../helpers/hash-helper");

const MockUserLogin = require("../mocks/user-login.json");

chai.use(chaiHttp);
chai.should();
const expect = chai.expect;

let mockUserLogin;

let stubUserFindOne;
let stubHashHelper;

describe("User Test", () => {
  describe("POST /user/v1/login", () => {
    beforeEach((done) => {
      mockUserLogin = _.cloneDeep(MockUserLogin);
      done();
    });

    afterEach((done) => {
      if (stubUserFindOne) stubUserFindOne.restore();
      if (stubHashHelper) stubHashHelper.restore();
      done();
    });

    it("positive case: it should responds with 200, return new token", (done) => {
      stubUserFindOne = sinon.stub(User, "findOne").callsFake(() => {
        return mockUserLogin;
      });

      mockUserLogin.dataValues = { ...mockUserLogin };

      stubHashHelper = sinon
        .stub(HashHelper, "compareHashedPassword")
        .callsFake(() => {
          return true;
        });

      const dataPost = {
        email: "wade5@yahoo.co.id",
        password: "passwordbenar",
      };

      chai
        .request(app)
        .post("/user/v1/login")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(200);
          expect(response.body.message).to.equal("Login successfully");
          response.body.should.have.property("data").and.to.be.a("object");
          response.body.data.should.have
            .property("token")
            .and.to.be.a("string");
          response.body.data.should.not.have.property("password");
          done();
        });
    });

    it("negative case: it should responds with 401, wrong password", (done) => {
      stubUserFindOne = sinon.stub(User, "findOne").callsFake(() => {
        return mockUserLogin;
      });

      mockUserLogin.dataValues = { ...mockUserLogin };

      stubHashHelper = sinon
        .stub(HashHelper, "compareHashedPassword")
        .callsFake(() => {
          return false;
        });

      const dataPost = {
        email: "wade5@yahoo.co.id",
        password: "passwordsalah",
      };

      chai
        .request(app)
        .post("/user/v1/login")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.body.message).to.equal(
            "Sorry, email or password wrong"
          );
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 401, user not found", (done) => {
      stubUserFindOne = sinon.stub(User, "findOne").callsFake(() => {
        return {};
      });

      const dataPost = {
        email: "emailtidakada@yahoo.co.id",
        password: "bebasaja",
      };

      chai
        .request(app)
        .post("/user/v1/login")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(401);
          expect(response.body.message).to.equal("Sorry, user does not exist");
          response.body.should.not.have.property("data");
          done();
        });
    });

    it("negative case: it should responds with 406, missing mandatory field", (done) => {
      const dataPost = {
        email: "emailtanpapassword@yahoo.co.id",
      };

      chai
        .request(app)
        .post("/user/v1/login")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(406);
          response.body.message.should.be.an("array");
          done();
        });
    });

    it("negative case: it should responds with 500, something went wrong", (done) => {
      stubUserFindOne = sinon.stub(User, "findOne").callsFake(() => {
        return error;
      });

      const dataPost = {
        email: "emailtidakada@yahoo.co.id",
        password: "bebasaja",
      };

      chai
        .request(app)
        .post("/user/v1/login")
        .send(dataPost)
        .end((err, response) => {
          if (err) done(err);
          expect(response.status).to.equal(500);
          done();
        });
    });
  });
});
