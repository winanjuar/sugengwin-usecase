require("dotenv").config();
const getDefaultHeaders = () => {
  return {
    Authorization: `Bearer ${process.env.TOKEN_TEST}`,
  };
};

module.exports = {
  getDefaultHeaders,
};
