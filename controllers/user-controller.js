const { StatusCodes } = require("http-status-codes");

const ValidationHelper = require("../helpers/validation-helper");
const { errorValidationParser } = require("../helpers/parser-helper");
const hashHelper = require("../helpers/hash-helper");
const jwtHelper = require("../helpers/jwt-helper");
const { User } = require("../models");

class UserController {
  static async login(req, res) {
    try {
      let input = req.body;
      const validation = ValidationHelper.postLogin(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const user = await User.findOne({
        where: {
          email: input.email,
        },
        attributes: ["id", "email", "password"],
      });

      if (user && user.id) {
        const isAuthenticated = await hashHelper.compareHashedPassword(
          input.password,
          user.password
        );

        if (isAuthenticated) {
          const currentUser = { ...user.dataValues };
          delete currentUser.password;
          const token = jwtHelper.generateToken(currentUser);
          currentUser.token = token;

          res.status(StatusCodes.OK).json({
            message: "Login successfully",
            data: currentUser,
          });
        } else {
          res.status(StatusCodes.UNAUTHORIZED).json({
            message: "Sorry, email or password wrong",
          });
        }
      } else {
        res.status(StatusCodes.UNAUTHORIZED).json({
          message: "Sorry, user does not exist",
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }
}
module.exports = UserController;
