const { StatusCodes } = require("http-status-codes");

const ValidationHelper = require("../helpers/validation-helper");
const { errorValidationParser } = require("../helpers/parser-helper");
const { Product, Sequelize } = require("../models");
const Op = Sequelize.Op;

class ProductController {
  static async list(req, res) {
    try {
      let input = req.query;
      const validation = ValidationHelper.getProductList(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const limit = input.limit ? input.limit : 10;
      const page = input.page ? input.page : 1;
      const sortBy = input.sortBy ? input.sortBy : "name";
      const sortDir = input.sortDir ? input.sortDir : "ASC";

      const search = input.search ? input.search : null;
      const merchantId = input.merchantId ? input.merchantId : null;
      const priceFrom = input.priceFrom ? input.priceFrom : null;
      const priceTo = input.priceTo ? input.priceTo : null;
      const hasPromo = input.hasPromo ? input.hasPromo : null;

      const order = [
        [sortBy, sortDir],
        ["id", "ASC"],
      ];

      const condition = {};
      if (search)
        condition[Op.or] = [
          { name: { [Op.like]: "%" + search + "%" } },
          { description: { [Op.like]: "%" + search + "%" } },
        ];

      if (priceFrom && priceTo)
        condition.price = { [Op.between]: [priceFrom, priceTo] };

      if (merchantId) condition.merchantId = merchantId;
      if (hasPromo === "yes") condition["$promo.id$"] = { [Op.ne]: null };
      if (hasPromo === "no") condition["$promo.id$"] = { [Op.eq]: null };

      const productCount = await Product.count({
        where: condition,
        include: "promo",
      });

      const totalPages = Math.ceil(productCount / limit);
      const products = await Product.findAll({
        include: ["merchant", "promo"],
        where: condition,
        order,
        limit,
        offset: (page - 1) * limit,
      });
      if (products && products.length > 0) {
        res.status(StatusCodes.OK).json({
          message: `Get list product successfully`,
          totalData: productCount,
          totalPages: totalPages,
          returnData: products.length,
          currentPage: page,
          data: products,
        });
      } else {
        res.status(StatusCodes.OK).json({
          message: `Sorry, your filter return nothing`,
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }
}

module.exports = ProductController;
