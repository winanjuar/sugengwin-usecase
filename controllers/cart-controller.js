const { StatusCodes } = require("http-status-codes");

const ValidationHelper = require("../helpers/validation-helper");
const { errorValidationParser } = require("../helpers/parser-helper");
const { cartBuilder } = require("../helpers/builder-helper");
const { Cart, Product } = require("../models");

class CartController {
  static async list(req, res) {
    try {
      let input = {
        userId: req.user.id, //dapet dari session user
      };
      const validation = ValidationHelper.checkUserId(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const carts = await Cart.findAll({
        include: ["user", "merchant", "product", "promo"],
        where: { userId: input.userId },
      });

      if (carts && carts.length > 0) {
        res.status(StatusCodes.OK).json({
          message: `Get list product in cart successfully`,
          data: carts,
        });
      } else {
        res.status(StatusCodes.OK).json({
          message: `Sorry, your cart still empty`,
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  static async add(req, res) {
    try {
      let input = {
        userId: req.user.id, //dapet dari session user
        ...req.body, //contains productId, quantity
      };

      const validation = ValidationHelper.addToCart(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }

      input = validation.value;
      input.product = await Product.findByPk(input.productId, {
        include: "promo",
      });
      const promoDescription =
        input.product && input.product.promo && input.product.promo.description
          ? input.product.promo.description
          : null;

      const condition = {
        userId: input.userId,
        productId: input.productId,
        merchantId: input.product.merchantId,
      };

      const cart = await Cart.findOne({ where: condition });

      if (cart && cart.id) {
        input.quantity = cart.fixQuantity + input.quantity;
        const newCart = cartBuilder(input);
        await Cart.update(newCart, { where: condition });
        const updatedCart = await Cart.findByPk(cart.id);

        res.status(StatusCodes.OK).json({
          message: `Update product in cart successfully`,
          data: { cart: updatedCart, promo: promoDescription },
        });
      } else {
        const newCart = cartBuilder(input);
        const cart = await Cart.create(newCart);

        res.status(StatusCodes.OK).json({
          message: `Add product to cart successfully`,
          data: { cart, promo: promoDescription },
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }
}

module.exports = CartController;
