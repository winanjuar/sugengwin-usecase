const { StatusCodes } = require("http-status-codes");
const crypto = require("crypto");

const ValidationHelper = require("../helpers/validation-helper");
const { errorValidationParser } = require("../helpers/parser-helper");
const { moment, randInt, isAllMember } = require("../helpers/general-helper");
const { Order, OrderItem, Product, Promo, Cart } = require("../models");

class OrderController {
  static async list(req, res) {
    try {
      let input = {
        userId: req.user.id, //dapet dari session user
      };
      const validation = ValidationHelper.checkUserId(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const orders = await Order.findAll({
        where: { userId: input.userId },
        order: [["createdAt", "DESC"]],
      });
      if (orders && orders.length > 0) {
        res.status(StatusCodes.OK).json({
          message: `Get list transaction successfully`,
          data: orders,
        });
      } else {
        res.status(StatusCodes.OK).json({
          message: `Sorry, your does not have transaction`,
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  static async detail(req, res) {
    try {
      let input = {
        userId: req.user.id, //dapet dari session user
        orderId: req.params.id, //dapet dari parameter
      };
      const validation = ValidationHelper.getOrderDetail(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const order = await Order.findOne({
        where: { id: input.orderId, userId: input.userId },
        include: [
          "user",
          {
            model: OrderItem,
            as: "orderItems",
            include: [
              "merchant",
              {
                model: Product,
                as: "product",
                attributes: ["id", "name", "description", "image"],
              },
              {
                model: Promo,
                as: "promo",
                attributes: ["id", "description"],
              },
            ],
          },
        ],
      });
      if (order && order.id) {
        res.status(StatusCodes.OK).json({
          message: `Get order successfully`,
          data: order,
        });
      } else {
        res.status(StatusCodes.OK).json({
          message: `Sorry, order not found`,
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }

  static async checkout(req, res) {
    try {
      let input = {
        userId: req.user.id, //dapet dari session user
        cartIds: req.body.cartIds, //dapet dari body
      };
      const validation = ValidationHelper.checkout(input);
      if (validation.error) {
        const errorDetails = errorValidationParser(validation.error);
        res.status(StatusCodes.NOT_ACCEPTABLE).json({ message: errorDetails });
        return;
      }
      input = validation.value;

      const allCarts = await Cart.findAll({
        where: { userId: input.userId },
      });

      const allCartIds = allCarts.map((cart) => cart.id);
      const isCartValid = isAllMember(input.cartIds, allCartIds);

      if (isCartValid) {
        const selectedCarts = allCarts.filter((cart) =>
          input.cartIds.includes(cart.id)
        );

        const orderId = crypto.randomUUID();
        const newOrderItems = selectedCarts.map((selectedCart) => {
          return {
            id: crypto.randomUUID(),
            orderId,
            infoCartId: selectedCart.id,
            merchantId: selectedCart.merchantId,
            productId: selectedCart.productId,
            promoId: selectedCart.promoId,
            price: selectedCart.price,
            quantity: selectedCart.quantity,
            fixBuy: selectedCart.fixBuy,
            free: selectedCart.free,
            fixFree: selectedCart.fixFree,
            fixQuantity: selectedCart.fixQuantity,
            bruto: selectedCart.bruto,
            discount: selectedCart.discount,
            netto: selectedCart.netto,
          };
        });
        const grandBruto = newOrderItems.reduce(
          (total, orderItem) => total + orderItem.bruto,
          0
        );

        const grandDiscount = newOrderItems.reduce(
          (total, orderItem) => total + orderItem.discount,
          0
        );

        const grandNetto = grandBruto - grandDiscount;

        const newOrder = {
          id: orderId,
          userId: input.userId,
          orderNumber: `ORD${moment()}${randInt()}`,
          orderTime: new Date(),
          grandBruto,
          grandDiscount,
          grandNetto,
        };

        const order = await Order.create(newOrder);
        const orderItems = await OrderItem.bulkCreate(newOrderItems);
        await Cart.destroy({ where: { id: input.cartIds } });

        res.status(StatusCodes.OK).json({
          message: `Checkout successfully`,
          data: { order, orderItems },
        });
      } else {
        res.status(StatusCodes.UNAUTHORIZED).json({
          message: `Sorry, checkout failed`,
        });
      }
    } catch (error) {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR).json(error);
    }
  }
}

module.exports = OrderController;
